package net.atos.iot.tests.postgreTest01;

import net.atos.iot.tests.postgreTest01.logic.IotService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostgreTestApplicationTests {

    @Autowired
    private IotService service;
            
    @Test
    public void contextLoads() {
        Long count = service.getCount();
        System.out.println(">>>> count = " + count);
    }

}
