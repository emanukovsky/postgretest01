package net.atos.iot.tests.postgreTest01.logic;

import javax.annotation.PostConstruct;
import net.atos.iot.tests.postgreTest01.model.Device;
import net.atos.iot.tests.postgreTest01.repository.IotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IotServiceImpl implements IotService{
    
    @Autowired
    private IotRepository repository;

    @Override
    @PostConstruct
    public void initDb(){
        long counter = getCount();
        if (counter == 0){
            generateDevices();
        }
    }
    
    @Transactional
    private Device generateDevice(int number) {
        Device newDevice = new Device(null, "sn_" + number, "type_" + number, 
            "name_" + number, "ip_" + number, "version_" + number);
        return newDevice;
    }
    
    private void generateDevices(){
        for (int i = 0; i <= 10; i++) {
            repository.save(generateDevice(i));
        }
    }
    
    @Override
    public Long getCount(){
        return repository.count();
    }

}
