package net.atos.iot.tests.postgreTest01.repository;

import net.atos.iot.tests.postgreTest01.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IotRepository extends JpaRepository<Device, Long>{
    
}
