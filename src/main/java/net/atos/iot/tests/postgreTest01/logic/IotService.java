package net.atos.iot.tests.postgreTest01.logic;

import org.springframework.stereotype.Service;

@Service
public interface IotService{
    
    public void initDb();
    public Long getCount();
    
}
