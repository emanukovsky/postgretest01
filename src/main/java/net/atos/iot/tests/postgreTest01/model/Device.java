package net.atos.iot.tests.postgreTest01.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Device")
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "sn", nullable = false)
    private String sn;
    
    @Column(name = "type", nullable = false)
    private String type;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "ip", nullable = false)
    private String ip;
    
    @Column(name = "version", nullable = false)
    private String version;

    public Device(){
        
    }
    
    public Device(Long id, String sn, String type, String name, String ip, 
            String version){
        this.id = id;
        this.sn = sn; 
        this.type = type; 
        this.name = name; 
        this.ip = ip; 
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
