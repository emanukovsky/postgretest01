package net.atos.iot.tests.postgreTest01.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import net.atos.iot.tests.postgreTest01.exception.ResourceNotFoundException;
import net.atos.iot.tests.postgreTest01.model.Device;
import net.atos.iot.tests.postgreTest01.repository.IotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/iot")
public class IotController {

    @Autowired
    private IotRepository repository;

    /* get all devices */
    @GetMapping("/devices")
    public List<Device> getAllDevices() {
        return repository.findAll();
    }
    
    /* get device by id */
    @GetMapping("/device/{id}")
    public ResponseEntity<Device> getDeviceById(@PathVariable(value = "id") Long deviceId)
        throws ResourceNotFoundException {
        Device device = repository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException(deviceId));
        return ResponseEntity.ok().body(device);
    }
    
    /* add new device */
    @PostMapping("/device")
    public Device createDevice(@Valid @RequestBody Device device) {
        return repository.save(device);
    }

    /* update device by id */
    @PutMapping("/device/{id}")
    public ResponseEntity<Device> updateDevice(@PathVariable(value = "id") Long deviceId,
         @Valid @RequestBody Device device) throws ResourceNotFoundException {
        Device deviceToUpdate = repository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException(deviceId));
        deviceToUpdate.setIp(device.getIp());
        deviceToUpdate.setName(device.getName());
        deviceToUpdate.setSn(device.getSn());
        deviceToUpdate.setType(device.getType());
        deviceToUpdate.setVersion(device.getVersion());
        final Device updatedDevice = repository.save(deviceToUpdate);
        return ResponseEntity.ok(updatedDevice);
    }

    /* delete device by id */
    @DeleteMapping("/device/{id}")
    public Map<String, Boolean> deleteDevice(@PathVariable(value = "id") Long deviceId)
         throws ResourceNotFoundException {
        Device device = repository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException(deviceId));
        repository.delete(device);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    
}
